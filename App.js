import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import Body from "./src/body";
import Header from "./src/header";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      tareas: [],
      texto: ""
    };
  }

  establecerTexto = (value) =>{
    console.log(value);
    //this.setState({texto:value});
  }

    render() {
  return (
    <View style={styles.container}>
      <Header cambiarTexto={this.establecerTexto}/>
      <Body />
    </View>
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default App;