import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

class Header extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Text> Esto es Header</Text>
                <TextInput
                    style={styles.texto}
                    onChangeText={this.props.cambiarTexto}
                    placeholder="insertar texto"
                />
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        backgroundColor: 'blue',
        justifyContent: 'center',
    },
    texto: {
        paddingHorizontal: 16,
    }
});

export default Header;